import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.util.Scanner;

public class Assignment {
    private JButton tempuraButton;
    private JButton pizzaButton;
    private JTextPane textPane1;
    private JPanel root;
    private JButton friedRiceButton;
    private JButton sobaButton;
    private JButton spaghettiButton;
    private JButton ramenButton;
    private JTextPane textPane2;
    private JButton checkOutButton;

    int total = 0;

    void order(String food, int yen) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food + "　　　" + yen + "yen\n");
            total += yen;
            textPane2.setText("Total   " + total + "yen");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment");
        frame.setContentPane(new Assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public Assignment() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 700);

            }
        });
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("tempura.jpeg")));

        pizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pizza", 900);
            }
        });
        pizzaButton.setIcon(new ImageIcon(this.getClass().getResource("pizza.jpg")));

        friedRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fried rice", 600);
            }
        });
        friedRiceButton.setIcon(new ImageIcon(this.getClass().getResource("friedrice.jpg")));

        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba", 1000);
            }
        });
        sobaButton.setIcon(new ImageIcon(this.getClass().getResource("soba.jpeg")));

        spaghettiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Spaghetti", 1100);
            }
        });
        spaghettiButton.setIcon(new ImageIcon(this.getClass().getResource("spaghetti.jpg")));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 800);
            }
        });
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkout = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Order check out",
                        JOptionPane.YES_NO_OPTION);

                if (checkout == 0) {
                    int pointcard = JOptionPane.showConfirmDialog(null,
                            "Do you have a point card?",
                            "Point card",
                            JOptionPane.YES_NO_OPTION);

                    if (pointcard == 0) {
                        int numbercheck = -1;
                        while (numbercheck != 0) {
                            String cardnumber = JOptionPane.showInputDialog("Please enter your card number.");
                            if (cardnumber == null) {
                                JOptionPane.showMessageDialog(null, "Thank you for ordering! The total price is " + total + "yen.\n"
                                        + "It will be served as soon as possible.");
                                break;
                            } else {
                                numbercheck = JOptionPane.showConfirmDialog(null,
                                        "Is this number correct? " + cardnumber,
                                        "Check",
                                        JOptionPane.YES_NO_OPTION);
                                if (numbercheck == 0) {
                                    int point = total / 1000;
                                    JOptionPane.showMessageDialog(null, "Thank you for ordering! The total price is " + total + "yen.\n"
                                            + "You got " + point + "points. (1 point / 1000 yen)\n" +
                                            "It will be served as soon as possible.");
                                }
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Thank you for ordering! The total price is " + total + "yen.\n"
                                + "It will be served as soon as possible.");
                    }
                    total = 0;
                    textPane1.setText("");
                    textPane2.setText("Total");
                }
            }
        });
    }
}
